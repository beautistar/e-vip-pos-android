<?php

class Api_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    function usernameExist($name) {

        $this->db->where('name', $name);         
        return $this->db->get('tb_user')->num_rows();
    }

    function emailExist($email) {

        $this->db->where('email', $email);
        return $this->db->get('tb_user')->num_rows();
    }
    
    function register($name, $email, $password) {

        $this->db->set('name', $name);
        $this->db->set('email', $email);        
        $this->db->set('password', $password);
        $this->db->insert('tb_user');
        return $this->db->insert_id();
    }
    
    function updatePhoto($user_id, $file_url) {
        
        $this->db->where('id', $user_id);
        $this->db->set('photo_url', $file_url);
        $this->db->update('tb_user');
    } 
    
}
?>
