<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller  {


    function __construct(){

        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('api_model');
        $this->load->library('session');     
    }
    
    /**
     * Make json response to the client with result code message
     *
     * @param p_result_code : Result code
     * @param p_result_msg : Result message
     * @param p_result : Result json object
     */

     private function doRespond($p_result_code,  $p_result){

         $p_result['result_code'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
     }

     /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

     private function doRespondSuccess($result){

         $this->doRespond(0, $result);
     }

    /**
     * Url decode.
     *
     * @param p_text : Data to decode
     *
     * @return text : Decoded text
     */

     private function doUrlDecode($p_text){

        $p_text = urldecode($p_text);
        $p_text = str_replace('&#40;', '(', $p_text);
        $p_text = str_replace('&#41;', ')', $p_text);
        $p_text = str_replace('%40', '@', $p_text);
        $p_text = str_replace('%20',' ', $p_text);
        $p_text = trim($p_text);


        return $p_text;
     }
       
     
     public function register($name, $email, $password) {

         $result = array();

         $name = $this->doUrlDecode($name);
         $email = $this->doUrlDecode($email);          
         $password = $this->doUrlDecode($password);

         $usernameExist = $this->api_model->usernameExist($name);
         $emailExist = $this->api_model->emailExist($email);

         if($usernameExist > 0) { // username already exist

             $this->doRespond(101, $result);
             return;
         }

         if ($emailExist > 0) {

             $this->doRespond(102, $result);
             return;
         }
         
         if(!function_exists('password_hash'))
             $this->load->helper('password');

         $password_hash = password_hash($password, PASSWORD_BCRYPT);

         $result['id'] = $this->api_model->register($name, $email, $password_hash);  

         $this->doRespondSuccess($result);

     }
     
     function uploadPhoto() {// baseurl wrong
         
         $result = array();

         $user_id = $_POST['id'];

         $upload_path = "uploadfiles/user/";  

         $upload_url = base_url()."uploadfiles/user/";

        // Upload file.

        $image_name = $user_id."_file";

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 4000,
            'max_height' => 3000,
            'file_name' => $image_name.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('file')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $this->api_model->updatePhoto($user_id, $file_url);
            $result['photo_url'] = $file_url;
            $this->doRespondSuccess($result);
            return;

        } else {

            $this->doRespond(103, $result);// upload fail
            return;
        }
     }
     
     function login($email, $password) { 
         
         $result = array();
         $email = $this->doUrlDecode($email);
         $password = $this->doUrlDecode($password);
                           
         $is_exist = $this->api_model->emailExist($email);

         if ($is_exist == 0) {
             $this->doRespond(104, $result);  // Unregistered email.
             return;
         }

         if(!function_exists('password_verify'))
             $this->load->helper('password'); // password_helper.php loading

         $row = $this->db->get_where('tb_user', array('email'=>$email))->row();
         $pwd = $row->password;

         if (!password_verify($password, $pwd)){  // wrong password.

             $this->doRespond(105, $result);
             return;

         } else {
             
             $result['user_info'] = array('idx' => $row->id,
                                          'name' => $row->name,                                          
                                          'email' => $row->email,
                                          'label' => $row->label,
                                          'bg_url' => $row->bg_url,
                                          'photo_url' => $row->photo_url);                         
         }

         $this->doRespondSuccess($result);  
         
     }
                          
}

?>
