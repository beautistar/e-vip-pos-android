package com.e_vipshopper.main;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.e_vipshopper.R;
import com.e_vipshopper.utils.AlphaForeGroundColorSpan;
import com.e_vipshopper.utils.ScrollViewHelper;

public class ProductInfoActivity extends ActionBarActivity implements View.OnClickListener{

    ColorDrawable cd;
    private AlphaForeGroundColorSpan mAlphaForegroundColorSpan;
    private SpannableString mSpannableString;

    Toolbar toolbar;
    ActionBar actionBar;
    ScrollViewHelper ui_scroll_view;
    LinearLayout ui_lyt_show_map, ui_lyt_feedback, ui_lyt_phone;

    String title = "Coffee";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_info);

        loadLayout();
    }

    @SuppressLint("NewApi")
    private void loadLayout() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        cd = new ColorDrawable(getResources().getColor(R.color.colorPrimary));

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("");
        actionBar.setBackgroundDrawable(cd);

        cd.setAlpha(0);

        ui_scroll_view = (ScrollViewHelper)findViewById(R.id.scroll_view);
        ui_scroll_view.setOnScrollViewListener(new ScrollViewHelper.OnScrollViewListener() {
            @Override
            public void onScrollChanged(ScrollViewHelper v, int l, int t, int oldl, int oldt) {

                setTitleAlpha(255 - getAlphaforActionBar(v.getScrollY()));
                cd.setAlpha(getAlphaforActionBar(v.getScrollY()));
            }

            private int getAlphaforActionBar(int scrollY) {
                int minDist = 0, maxDist = 550;
                if(scrollY>maxDist){
                    return 255; }
                else {
                    if (scrollY < minDist) {
                        return 0;
                    } else {
                        return (int) ((255.0 / maxDist) * scrollY);
                    }
                }
            }

        });

        mSpannableString = new SpannableString(title);
        mAlphaForegroundColorSpan = new AlphaForeGroundColorSpan(0xFFFFFF);

        ui_lyt_show_map = (LinearLayout)findViewById(R.id.lyt_show_map);
        ui_lyt_show_map.setOnClickListener(this);

        ui_lyt_feedback = (LinearLayout)findViewById(R.id.lyt_feedback);
        ui_lyt_feedback.setOnClickListener(this);

        ui_lyt_phone = (LinearLayout)findViewById(R.id.lyt_phone);
        ui_lyt_phone.setOnClickListener(this);
    }

    private void setTitleAlpha(float alpha) {
        if(alpha<1){ alpha = 1; }
        mAlphaForegroundColorSpan.setAlpha(alpha);
        mSpannableString.setSpan(mAlphaForegroundColorSpan, 0, mSpannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(mSpannableString);
    }

    private void gotoMapActivity() {

        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }

    private void giveFeedBack() {

        LayoutInflater inflater =LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.alert_feedback, null);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(view);
        final AlertDialog dialog = alert.create();
        dialog.show();

        TextView ui_txv_cancel = (TextView)view.findViewById(R.id.txv_cancel);
        ui_txv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();
            }
        });
    }

    private void callPhone() {

        LayoutInflater inflater =LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.alert_call, null);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(view);
        final AlertDialog dialog = alert.create();
        dialog.show();

        TextView ui_txv_cancel = (TextView)view.findViewById(R.id.txv_cancel);
        ui_txv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.lyt_show_map:
                gotoMapActivity();
                break;

            case R.id.lyt_feedback:
                giveFeedBack();
                break;

            case R.id.lyt_phone:
                callPhone();
                break;
        }
    }

}
