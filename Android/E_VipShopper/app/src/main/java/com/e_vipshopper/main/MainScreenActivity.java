package com.e_vipshopper.main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.e_vipshopper.R;
import com.e_vipshopper.base.CommonActivity;
import com.e_vipshopper.fragments.HelpFragment;
import com.e_vipshopper.fragments.MyProfileFragment;
import com.e_vipshopper.fragments.NotificationFragment;
import com.e_vipshopper.fragments.PlaceFragment;
import com.e_vipshopper.fragments.QRCodeFragment;
import com.e_vipshopper.fragments.ReceiptFragment;
import com.e_vipshopper.fragments.ScanFragment;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class MainScreenActivity extends CommonActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    NavigationView ui_drawer_menu;
    DrawerLayout ui_drawerlayout;
    ActionBarDrawerToggle drawerToggle;
    ImageView ui_imv_call_drawer, ui_imv_photo;
    View header_view;
    TextView ui_txv_title, ui_txv_qrcode;

    public String qr_code = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);

        loadLayout();
    }

    public String getQr_code() {
        return qr_code;
    }

    public void setQr_code(String qr_code) {
        this.qr_code = qr_code;
    }

    private void loadLayout() {

        ui_txv_title = (TextView)findViewById(R.id.txv_title);

        ui_drawer_menu = (NavigationView)findViewById(R.id.drawer_menu);
        ui_drawer_menu.setNavigationItemSelectedListener(this);
        header_view = ui_drawer_menu.getHeaderView(0);

        ui_imv_photo = (ImageView)header_view.findViewById(R.id.imv_photo);
        ui_imv_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectItem();
            }
        });

        ui_drawerlayout = (DrawerLayout)findViewById(R.id.drawerlayout);

        ui_imv_call_drawer = (ImageView)findViewById(R.id.imv_call_drawer);
        ui_imv_call_drawer.setOnClickListener(this);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setupNavigationBar();

        ui_drawerlayout.setDrawerListener(drawerToggle);

        loadPlace();
    }

    private void selectItem() {

        final String[] items = {"Profile", "Exit", "Remove account", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {

                switch (item){

                    case 0:
                        loadProfile();
                        break;
                    default:
                        loadPlace();
                        break;
                }
                ui_drawerlayout.closeDrawers();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void setupNavigationBar() {

        drawerToggle = new ActionBarDrawerToggle(this,ui_drawerlayout, R.string.drawer_open, R.string.drawer_close){
            @Override
            public void onDrawerOpened(View drawerView) {

                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {

                super.onDrawerClosed(drawerView);
            }
        };

        ui_drawerlayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    public void loadPlace() {

        ui_txv_title.setText(getString(R.string.my_place));

        PlaceFragment fragment = new PlaceFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment);
        fragmentTransaction.commit();
    }


    public void loadQrCode() {

        ui_txv_title.setText(getString(R.string.my_qrcode));

        QRCodeFragment fragment = new QRCodeFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment);
        fragmentTransaction.commit();
    }

    public void loadScan() {

        ui_txv_title.setText(getString(R.string.scan));

        ScanFragment fragment = new ScanFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment);
        fragmentTransaction.commit();
    }

    public void loadReceipts() {

        ui_txv_title.setText(getString(R.string.my_receipts));

        ReceiptFragment fragment = new ReceiptFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment);
        fragmentTransaction.commit();
    }

    public void loadNotification() {

        ui_txv_title.setText(getString(R.string.notification));

        NotificationFragment fragment = new NotificationFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment);
        fragmentTransaction.commit();
    }

    public void loadProfile() {

        ui_txv_title.setText(getString(R.string.my_profile));

        MyProfileFragment fragment = new MyProfileFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment);
        fragmentTransaction.commit();
    }

    public void loadHelp() {

        ui_txv_title.setText(getString(R.string.help));

        HelpFragment fragment = new HelpFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Log.d("MainActivity", "Cancelled scan");
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                Log.d("MainActivity", "Scanned");
                qr_code = result.getContents();
//                Toast.makeText(this, "Scanned: " + qr_code , Toast.LENGTH_LONG).show();

            }
        } else {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_call_drawer:
                ui_drawerlayout.openDrawer(Gravity.LEFT);
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected( MenuItem item) {

        int id = item.getItemId();

        if (item.isChecked()) item.setChecked(false); else item.setChecked(true);
        ui_drawerlayout.closeDrawers();

        switch (id){

            case R.id.place:
                loadPlace();
                break;

            case R.id.qrcode:
                loadQrCode();
                break;

            case R.id.scan:
                loadScan();
                break;

            case R.id.receipts:
                loadReceipts();
                break;

            case R.id.notifications:
                loadNotification();
                break;

            case R.id.profile:
                loadProfile();
                break;

            case R.id.help:
                loadHelp();
                break;
        }

        return true;
    }
}
