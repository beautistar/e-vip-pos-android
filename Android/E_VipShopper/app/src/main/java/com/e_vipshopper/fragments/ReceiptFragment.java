package com.e_vipshopper.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.e_vipshopper.R;
import com.e_vipshopper.adapter.ReceiptAdapter;
import com.e_vipshopper.base.BaseFragment;
import com.e_vipshopper.main.MainScreenActivity;

/**
 * Created by STS on 10/18/2016.
 */

@SuppressLint("ValidFragment")
public class ReceiptFragment extends BaseFragment {

    ReceiptAdapter adapter;

    MainScreenActivity activity;

    View view;
    ListView ui_lst_receipts;

    public ReceiptFragment(MainScreenActivity activity) {
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_receipts, container, false);

        loadLayout();

        return view;
    }

    private void loadLayout() {

        adapter = new ReceiptAdapter(activity);
        ui_lst_receipts = (ListView)view.findViewById(R.id.lst_receipts);
        ui_lst_receipts.setAdapter(adapter);
    }
}
