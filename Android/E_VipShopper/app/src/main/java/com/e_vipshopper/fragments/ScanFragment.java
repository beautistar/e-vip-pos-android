package com.e_vipshopper.fragments;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.e_vipshopper.R;
import com.e_vipshopper.base.BaseFragment;
import com.e_vipshopper.main.MainScreenActivity;
import com.e_vipshopper.utils.Contents;
import com.e_vipshopper.utils.QRCodeEncoder;
import com.e_vipshopper.utils.ToolbarCaptureActivity;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.integration.android.IntentIntegrator;

import static android.content.Context.WINDOW_SERVICE;

/**
 * Created by STS on 10/18/2016.
 */

@SuppressLint("ValidFragment")
public class ScanFragment extends BaseFragment implements View.OnClickListener{

    MainScreenActivity activity;

    View view;
    LinearLayout ui_lyt_place, ui_lyt_code, ui_lyt_scan;
    ImageView ui_imv_scan, ui_imv_scanner;
    TextView ui_txv_scan;

    String qr_code = "";

    Bundle savedInstanceState;

    public ScanFragment(MainScreenActivity activity) {
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_scan, container, false);
        loadLayout();

        return view;
    }

    private void loadLayout() {

        ui_imv_scanner = (ImageView)view.findViewById(R.id.imv_qrcode);
        ui_imv_scanner.setOnClickListener(this);

        ui_lyt_scan = (LinearLayout)view.findViewById(R.id.lyt_scan);
        ui_lyt_scan.setBackgroundColor(getResources().getColor(R.color.colorBlue));

        ui_imv_scan = (ImageView)view.findViewById(R.id.imv_scan);
        ui_imv_scan.setImageResource(R.drawable.photo_camera_white);

        ui_txv_scan = (TextView)view.findViewById(R.id.txv_scan);
        ui_txv_scan.setTextColor(getResources().getColor(R.color.colorWhite));

        ui_lyt_place = (LinearLayout)view.findViewById(R.id.lyt_place);
        ui_lyt_place.setOnClickListener(this);

        ui_lyt_code = (LinearLayout)view.findViewById(R.id.lyt_code);
        ui_lyt_code.setOnClickListener(this);
    }

    @Override
    public void onResume() {

        super.onResume();

        qr_code = activity.getQr_code();
        if (qr_code.length() > 0){

            activity.showAlertDialog(qr_code);

            WindowManager manager = (WindowManager)activity.getSystemService(WINDOW_SERVICE);
            Display display = manager.getDefaultDisplay();
            Point point = new Point();
            display.getSize(point);
            int width = point.x;
            int height = point.y;
            int smallerDimension = width < height ? width : height;
            smallerDimension = smallerDimension * 3/4;

            //Encode with a QR Code image
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qr_code,
                    null,
                    Contents.Type.TEXT,
                    BarcodeFormat.QR_CODE.toString(),
                    smallerDimension);
            try {
                Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
                ImageView myImage = (ImageView)view.findViewById(R.id.imv_qrcode);
                myImage.setImageBitmap(bitmap);

            } catch (WriterException e) {
                e.printStackTrace();
            }

        }

        qr_code = "";
        activity.setQr_code("");
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.lyt_place:
                activity.loadPlace();
                break;

            case R.id.lyt_code:
                activity.loadQrCode();
                break;

            case R.id.imv_qrcode:
                new IntentIntegrator(activity).setCaptureActivity(ToolbarCaptureActivity.class).initiateScan();
                break;
        }
    }

}
