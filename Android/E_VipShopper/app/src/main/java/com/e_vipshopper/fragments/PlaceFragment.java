package com.e_vipshopper.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.e_vipshopper.R;
import com.e_vipshopper.adapter.PlaceAdapter;
import com.e_vipshopper.base.BaseFragment;
import com.e_vipshopper.main.MainScreenActivity;

/**
 * Created by STS on 10/18/2016.
 */

@SuppressLint("ValidFragment")
public class PlaceFragment extends BaseFragment implements View.OnClickListener{

    PlaceAdapter adapter;

    MainScreenActivity activity;

    View view;
    GridView ui_grd_view;
    LinearLayout ui_lyt_place, ui_lyt_code, ui_lyt_scan;
    ImageView ui_imv_place;
    TextView ui_txv_place;

    public PlaceFragment(MainScreenActivity activity) {
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_place, container, false);
        loadLayout();

        return view;
    }

    private void loadLayout() {

        ui_grd_view = (GridView)view.findViewById(R.id.grd_view);
        adapter = new PlaceAdapter(activity);
        ui_grd_view.setAdapter(adapter);

        ui_lyt_code = (LinearLayout)view.findViewById(R.id.lyt_code);
        ui_lyt_code.setOnClickListener(this);

        ui_lyt_scan = (LinearLayout)view.findViewById(R.id.lyt_scan);
        ui_lyt_scan.setOnClickListener(this);

        ui_imv_place = (ImageView)view.findViewById(R.id.imv_place);
        ui_imv_place.setImageResource(R.drawable.favorite_white);

        ui_txv_place = (TextView)view.findViewById(R.id.txv_place);
        ui_txv_place.setTextColor(getResources().getColor(R.color.colorWhite));

        ui_lyt_place = (LinearLayout)view.findViewById(R.id.lyt_place);
        ui_lyt_place.setBackgroundColor(getResources().getColor(R.color.colorBlue));

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.lyt_code:
                activity.loadQrCode();
                break;

            case R.id.lyt_scan:
                activity.loadScan();
                break;
        }
    }

}
