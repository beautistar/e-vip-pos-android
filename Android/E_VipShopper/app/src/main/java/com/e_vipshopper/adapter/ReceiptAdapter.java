package com.e_vipshopper.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.e_vipshopper.R;
import com.e_vipshopper.main.MainScreenActivity;
import com.e_vipshopper.main.MyProductActivity;

/**
 * Created by STS on 10/18/2016.
 */

public class ReceiptAdapter extends BaseAdapter {

    MainScreenActivity activity;

    public ReceiptAdapter(MainScreenActivity activity) {
        this.activity = activity;
    }


    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null){

            LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_receipt, parent, false);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, MyProductActivity.class);
                activity.startActivity(intent);
            }
        });

        return convertView;
    }
}
