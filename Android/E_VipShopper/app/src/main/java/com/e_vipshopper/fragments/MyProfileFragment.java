package com.e_vipshopper.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.e_vipshopper.R;
import com.e_vipshopper.base.BaseFragment;
import com.e_vipshopper.main.MainScreenActivity;

/**
 * Created by STS on 10/18/2016.
 */

@SuppressLint("ValidFragment")
public class MyProfileFragment extends BaseFragment {

    MainScreenActivity activity;

    View view;

    public MyProfileFragment(MainScreenActivity activity) {
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_myprofile, container, false);

        loadLayout();

        return view;
    }

    private void loadLayout() {


    }
}
