package com.e_vipshopper.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.e_vipshopper.R;
import com.e_vipshopper.base.BaseFragment;
import com.e_vipshopper.main.MainScreenActivity;

/**
 * Created by STS on 10/18/2016.
 */

@SuppressLint("ValidFragment")
public class QRCodeFragment extends BaseFragment implements View.OnClickListener {

    MainScreenActivity activity;

    View view;
    LinearLayout ui_lyt_code, ui_lyt_place, ui_lyt_scan;
    ImageView ui_imv_code, ui_imv_my_code;
    TextView ui_txv_code;

    public QRCodeFragment(MainScreenActivity activity) {
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_qr_code, container, false);
        laodLayout();

        return view;
    }

    private void laodLayout() {

        ui_lyt_code = (LinearLayout)view.findViewById(R.id.lyt_code);
        ui_lyt_code.setBackgroundColor(getResources().getColor(R.color.colorBlue));

        ui_imv_code = (ImageView)view.findViewById(R.id.imv_code);
        ui_imv_code.setImageResource(R.drawable.qr_code_white);

        ui_txv_code = (TextView)view.findViewById(R.id.txv_code);
        ui_txv_code.setTextColor(getResources().getColor(R.color.colorWhite));

        ui_lyt_place = (LinearLayout)view.findViewById(R.id.lyt_place);
        ui_lyt_place.setOnClickListener(this);

        ui_lyt_scan = (LinearLayout)view.findViewById(R.id.lyt_scan);
        ui_lyt_scan.setOnClickListener(this);

        ui_imv_my_code = (ImageView)view.findViewById(R.id.imv_my_code);
        ui_imv_my_code.setOnClickListener(this);
    }

    private void showQRCode() {

        LayoutInflater inflater =LayoutInflater.from(activity);
        View view = inflater.inflate(R.layout.alert_qrcode, null);

        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setView(view);
        final AlertDialog dialog = alert.create();
        dialog.show();

        ImageView ui_imv_qrcode = (ImageView)view.findViewById(R.id.imv_qrcode);
        ui_imv_qrcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.lyt_place:
                activity.loadPlace();
                break;

            case R.id.lyt_scan:
                activity.loadScan();
                break;

            case R.id.imv_my_code:
                showQRCode();
                break;
        }
    }
}
