package com.e_vipshopper.utils;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.text.TextPaint;
import android.text.style.ForegroundColorSpan;

/**
 * Created by STS on 10/19/2016.
 */

@SuppressLint("ParcelCreator")
public class AlphaForeGroundColorSpan extends ForegroundColorSpan {

    private float mAlpha;

    public AlphaForeGroundColorSpan(int color) {
        super(color);
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        ds.setColor(getAlphaColor());
    }

    public void setAlpha(float alpha) {
        mAlpha = alpha;
    }

    public float getAlpha() {
        return mAlpha;
    }

    private int getAlphaColor() {
        int foregroundColor = getForegroundColor();
        return Color.argb((int) (mAlpha * 255), Color.red(foregroundColor), Color.green(foregroundColor), Color.blue(foregroundColor));
    }
}
