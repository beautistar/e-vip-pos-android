package com.e_vipshopper.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.e_vipshopper.R;
import com.e_vipshopper.base.CommonActivity;

public class TermsActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imv_back;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        loadLayout();
    }

    private void loadLayout() {

        ui_imv_back = (ImageView)findViewById(R.id.imv_back);
        ui_imv_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                finish();
                break;
        }
    }
}
