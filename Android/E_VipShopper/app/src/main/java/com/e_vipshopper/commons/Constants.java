package com.e_vipshopper.commons;

/**
 * Created by STS on 10/18/2016.
 */

public class Constants {

    public static final int SPLASH_TIME = 100;

    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_ALBUM = 101;
    public static final int CROP_FROM_CAMERA = 102;

    public static final int PROFILE_IMAGE_SIZE = 256;
}
