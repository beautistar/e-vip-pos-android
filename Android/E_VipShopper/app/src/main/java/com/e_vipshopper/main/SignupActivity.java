package com.e_vipshopper.main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.e_vipshopper.R;
import com.e_vipshopper.base.CommonActivity;
import com.e_vipshopper.commons.Constants;
import com.e_vipshopper.utils.BitmapUtils;

import java.io.File;
import java.io.InputStream;

import de.hdodenhof.circleimageview.CircleImageView;

public class SignupActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imv_cancel;
    CircleImageView ui_imv_photo;
    TextView ui_txv_next, ui_txv_terms;

    private Uri _imageCaptureUri;
    String _photoPath = "";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        loadLayout();
    }

    private void loadLayout() {

        ui_imv_photo = (CircleImageView)findViewById(R.id.imv_photo);
        ui_imv_photo.setOnClickListener(this);

        ui_imv_cancel = (ImageView)findViewById(R.id.imv_cancel);
        ui_imv_cancel.setOnClickListener(this);

        ui_txv_next = (TextView)findViewById(R.id.txv_next);
        ui_txv_next.setOnClickListener(this);

        ui_txv_terms = (TextView)findViewById(R.id.txv_terms);
        ui_txv_terms.setOnClickListener(this);
    }

    private void gotoLoginActivity() {

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);

        finish();
    }

    private void gotoMainPlacesActivity() {

        Intent intent = new Intent(this, MainScreenActivity.class);
        startActivity(intent);

        finish();
    }

    private void gotoTermsActivity() {

        Intent intent = new Intent(this, TermsActivity.class);
        startActivity(intent);

    }

    private void selectPhoto() {

        final String[] items = {"Take photo", "Choose from Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    doTakePhoto();

                } else {
                    doTakeGallery();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void doTakePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        _imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
    }

    private void doTakeGallery() {

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        ui_imv_photo.setImageBitmap(bitmap);
                        _photoPath = saveFile.getAbsolutePath();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    _imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    _photoPath = BitmapUtils.getRealPathFromURI(this, _imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(_imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(this)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_cancel:
                gotoLoginActivity();
                break;

            case R.id.txv_next:
                gotoMainPlacesActivity();
                break;

            case R.id.txv_terms:
                gotoTermsActivity();
                break;

            case R.id.imv_photo:
                selectPhoto();
                break;
        }
    }

}
