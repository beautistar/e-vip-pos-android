package com.e_vipshopper.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.widget.TextView;

import com.e_vipshopper.R;
import com.e_vipshopper.base.CommonActivity;

public class LoginActivity extends CommonActivity implements View.OnClickListener {

    TextView ui_txv_next, ui_txv_terms;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loadLayout();
    }

    private void loadLayout() {

        ui_txv_next = (TextView)findViewById(R.id.txv_next);
        ui_txv_next.setOnClickListener(this);

        ui_txv_terms = (TextView)findViewById(R.id.txv_terms);
        ui_txv_terms.setOnClickListener(this);
    }


    private void gotoSignupActivity() {

        Intent intent = new Intent(this, SignupActivity.class);
        startActivity(intent);

        finish();
    }

    private void gotoTermsActivity() {

        Intent intent = new Intent(this, TermsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.txv_next:
                gotoSignupActivity();
                break;

            case R.id.txv_terms:
                gotoTermsActivity();
                break;
        }
    }
}
