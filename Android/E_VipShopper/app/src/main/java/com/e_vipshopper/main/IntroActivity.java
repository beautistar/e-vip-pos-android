package com.e_vipshopper.main;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.e_vipshopper.R;
import com.e_vipshopper.base.CommonActivity;
import com.e_vipshopper.commons.Constants;

public class IntroActivity extends CommonActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        loadLayout();
    }

    private void loadLayout() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(IntroActivity.this, LoginActivity.class);

                startActivity(intent);
                finish();
            }
        }, Constants.SPLASH_TIME);
    }

}
