package com.e_vippos.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.e_vippos.R;
import com.e_vippos.base.CommonActivity;
import com.e_vippos.commons.Commons;

public class AddCustomerActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvBack, ui_imvOk;
    LinearLayout ui_lytOk;
    TextView ui_txvOk;
    EditText ui_edtName, ui_edtEmail, ui_edtNumber, ui_edtNote;

    String name, email, number, note;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_customer);

        loadLayout();
    }

    private void loadLayout() {

        ui_edtName = (EditText)findViewById(R.id.edtName);
        ui_edtEmail = (EditText)findViewById(R.id.edtEamil);
        ui_edtNumber = (EditText)findViewById(R.id.edtNumber);
        ui_edtNote = (EditText)findViewById(R.id.edtNote);

        ui_lytOk = (LinearLayout)findViewById(R.id.lytOk);
        ui_lytOk.setOnClickListener(this);

        ui_imvOk = (ImageView)findViewById(R.id.imvOk);
        ui_txvOk = (TextView)findViewById(R.id.txvOk);

        ui_imvBack = (ImageView)findViewById(R.id.imvBack);
        ui_imvBack.setOnClickListener(this);
    }

    private boolean check() {

        Boolean flag = false;

        name = ui_edtName.getText().toString();
        email = ui_edtEmail.getText().toString().trim();
        number = ui_edtNumber.getText().toString().trim();
        note = ui_edtNote.getText().toString().trim();

        if (name.length() == 0 && email.length() == 0 && number.length() == 0 && note.length() == 0){

            Toast toast = Toast.makeText(this,"At least one field must be filled in", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();

            return false;
        }

        return true;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imvBack:
                finish();
                break;

            case R.id.lytOk:
                if (check()){
                    ui_imvOk.setAlpha((float)1.0);
                    ui_txvOk.setAlpha((float)1.0);

                    Commons.addClientFlag = true;
                }

                break;
        }

    }

}
