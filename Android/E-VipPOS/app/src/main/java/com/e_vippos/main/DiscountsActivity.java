package com.e_vippos.main;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.e_vippos.R;
import com.e_vippos.base.CommonActivity;

public class DiscountsActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvBack;
    FloatingActionButton ui_fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discounts);

        loadLayout();
    }

    private void loadLayout() {

        ui_fab = (FloatingActionButton)findViewById(R.id.fab);
        ui_fab.setOnClickListener(this);

        ui_imvBack = (ImageView)findViewById(R.id.imvBack);
        ui_imvBack.setOnClickListener(this);
    }

    private void gotoCreateDiscountActivity() {

        Intent intent =  new Intent(this, CreateDiscountActivity.class);
        startActivity(intent);
    }
    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imvBack:
                finish();
                break;

            case R.id.fab:
                gotoCreateDiscountActivity();
                break;
        }
    }

}
