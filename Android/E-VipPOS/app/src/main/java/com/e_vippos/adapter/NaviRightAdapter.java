package com.e_vippos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.e_vippos.R;
import com.e_vippos.main.MainScreenActivity;

/**
 * Created by STS on 10/26/2016.
 */

public class NaviRightAdapter extends BaseAdapter {

    MainScreenActivity activity;

    public NaviRightAdapter(MainScreenActivity activity) {
        this.activity = activity;
    }


    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater =  (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convertView =  inflater.inflate(R.layout.item_navi_right, parent, false);

        return convertView;
    }
}
