package com.e_vippos.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.e_vippos.R;
import com.e_vippos.base.BaseFragment;
import com.e_vippos.main.MainScreenActivity;

/**
 * Created by STS on 10/23/2016.
 */

@SuppressLint("ValidFragment")
public class ReceiptsFragment extends BaseFragment {

    MainScreenActivity activity;

    View view;

    public ReceiptsFragment(MainScreenActivity activity) {
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_receipts, container, false);

        loadLayout();

        return view;
    }

    private void loadLayout() {


    }
}
