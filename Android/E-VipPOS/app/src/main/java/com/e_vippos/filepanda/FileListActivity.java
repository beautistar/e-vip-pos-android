package com.e_vippos.filepanda;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.e_vippos.R;
import com.e_vippos.commons.Constants;

import java.io.File;
import java.io.IOException;

public class FileListActivity extends FragmentActivity implements FileListFragment.Callbacks, View.OnClickListener {

    ImageView ui_imvBack;

    public static final int CODE_LIST = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_list);

        loadLayout();
    }

    private void loadLayout() {

        String root = getIntent().getStringExtra("root");

        FileListFragment fragment = (FileListFragment) getSupportFragmentManager().findFragmentById(R.id.file_list);

        fragment.setActivateOnItemClick(true);

        if (root != null) {
            fragment.loadFileDir(root);
        }

        ui_imvBack = (ImageView)findViewById(R.id.imvBack);
        ui_imvBack.setOnClickListener(this);
    }

    @Override
    public void onItemSelected(File file) {

        openFile(file);
    }

    public void openFile(File f) {

        if (f.isDirectory()) {

            Intent fileList = new Intent(this, FileListActivity.class);

            try {

                fileList.putExtra("root", f.getCanonicalPath());
                startActivityForResult(fileList, CODE_LIST);
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {

            Intent intent = new Intent();
            intent.putExtra(Constants.KEY_FILE, f.getAbsolutePath());
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case CODE_LIST:

                if (resultCode == RESULT_OK) {

                    setResult(RESULT_OK, data);
                    finish();
                }

                break;
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imvBack:
                finish();
                break;
        }
    }

}
