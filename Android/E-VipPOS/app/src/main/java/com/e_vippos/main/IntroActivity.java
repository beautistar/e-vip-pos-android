package com.e_vippos.main;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.e_vippos.R;
import com.e_vippos.commons.Constants;

public class IntroActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        loadLayout();
    }

    private void loadLayout() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(IntroActivity.this, LoginActivity.class);
//                Intent intent = new Intent(IntroActivity.this, MainScreenActivity.class);

                startActivity(intent);
                finish();
            }
        }, Constants.SPLASH_TIME);
    }
}
