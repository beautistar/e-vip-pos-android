package com.e_vippos.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.e_vippos.R;
import com.e_vippos.base.CommonActivity;

public class EditCustomerActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvBack, ui_imvOk;
    LinearLayout ui_lytOk;
    TextView ui_txvOk;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_customer);

        loadLayout();
    }

    private void loadLayout() {

        ui_txvOk = (TextView)findViewById(R.id.txvOk);
        ui_imvOk = (ImageView)findViewById(R.id.imvOk);

        ui_lytOk = (LinearLayout)findViewById(R.id.lytOk);
        ui_lytOk.setOnClickListener(this);

        ui_imvBack = (ImageView)findViewById(R.id.imvBack);
        ui_imvBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.lytOk:
                ui_txvOk.setAlpha((float)1.0);
                ui_imvOk.setAlpha((float)1.0);
                break;

            case R.id.imvBack:
                finish();
                break;
        }
    }
}
