package com.e_vippos.main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.e_vippos.R;
import com.e_vippos.adapter.ItemsAdapter;
import com.e_vippos.adapter.TicketAdapter;
import com.e_vippos.base.CommonActivity;
import com.e_vippos.utils.ToolbarCaptureActivity;
import com.google.zxing.integration.android.IntentIntegrator;

public class TicketActivity extends CommonActivity implements View.OnClickListener {

    TicketAdapter adapter;

    private PopupWindow mPopupWindow;

    ListView ui_lstTicket;
    ImageView ui_imvBack, ui_imvDelete, ui_imvAdd;
    LinearLayout ui_activity_ticket;
    View ui_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket);

        loadLayout();
    }

    private void loadLayout() {

        ui_view = (View)findViewById(R.id.view);

        ui_activity_ticket = (LinearLayout)findViewById(R.id.activity_ticket);

        ui_imvAdd = (ImageView)findViewById(R.id.imvAdd);
        ui_imvAdd.setOnClickListener(this);

        ui_imvDelete = (ImageView)findViewById(R.id.imvDelete);
        ui_imvDelete.setOnClickListener(this);

        ui_imvBack = (ImageView)findViewById(R.id.imvBack);
        ui_imvBack.setOnClickListener(this);

        adapter = new TicketAdapter(this);
        ui_lstTicket = (ListView)findViewById(R.id.lstTicket);
        ui_lstTicket.setAdapter(adapter);
    }

    private void showClearAlertDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Clear ticket");

        builder.setMessage("Are you sure you want to clear the ticket?")
                .setCancelable(false)
                .setPositiveButton("Clear", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();

        alert.show();
    }

    private void showPopupwindow() {

        LayoutInflater inflater = (LayoutInflater)_context.getSystemService(LAYOUT_INFLATER_SERVICE);

        View customView = inflater.inflate(R.layout.popup_add,null);

        mPopupWindow = new PopupWindow(
                customView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );

        mPopupWindow.showAsDropDown(ui_view);

        LinearLayout ui_lytContainer = (LinearLayout)customView.findViewById(R.id.lytAddContainer);
        ui_lytContainer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mPopupWindow.dismiss();
                return false;
            }
        });

        LinearLayout ui_lytScan = (LinearLayout)customView.findViewById(R.id.lytScan);
        ui_lytScan.setOnClickListener(this);

        LinearLayout ui_lytAdd = (LinearLayout)customView.findViewById(R.id.lytAdd);
        ui_lytAdd.setOnClickListener(this);
    }

    private void gotoAddCustomerActivity() {

        Intent intent = new Intent(this, AddCustomerActivity.class);
        startActivity(intent);
    }
    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imvBack:
                finish();
                break;

            case R.id.imvDelete:
                showClearAlertDialog();
                break;

            case R.id.imvAdd:
                showPopupwindow();
                break;

            case R.id.lytAdd:
                gotoAddCustomerActivity();
                break;

            case R.id.lytScan:
                new IntentIntegrator(this).setCaptureActivity(ToolbarCaptureActivity.class).initiateScan();
                break;
        }
    }

}
