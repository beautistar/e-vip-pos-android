package com.e_vippos.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.e_vippos.R;
import com.e_vippos.base.BaseFragment;
import com.e_vippos.main.MainScreenActivity;
import com.e_vippos.main.PrintersActivity;

/**
 * Created by STS on 10/23/2016.
 */

@SuppressLint("ValidFragment")
public class SettingsFragments extends BaseFragment implements View.OnClickListener{

    MainScreenActivity activity;

    View view;
    LinearLayout ui_lytPrinter;
    TextView ui_txvExit;

    public SettingsFragments(MainScreenActivity activity) {
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view =  inflater.inflate(R.layout.fragment_settings, container, false);

        loadLayout();

        return view;
    }

    private void loadLayout() {

        ui_lytPrinter = (LinearLayout)view.findViewById(R.id.lytPrinter);
        ui_lytPrinter.setOnClickListener(this);

        ui_txvExit = (TextView)view.findViewById(R.id.txvExit);
        ui_txvExit.setOnClickListener(this);
    }

    private void gotoPrintersActivity() {

        Intent intent =  new Intent(activity, PrintersActivity.class);
        activity.startActivity(intent);
    }

    private void showExitDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle("Sign out");

        builder.setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();

        alert.show();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.lytPrinter:
                gotoPrintersActivity();
                break;

            case R.id.txvExit:
                showExitDialog();
                break;
        }
    }

}
