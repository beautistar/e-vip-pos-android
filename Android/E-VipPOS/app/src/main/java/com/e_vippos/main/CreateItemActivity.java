package com.e_vippos.main;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.e_vippos.R;
import com.e_vippos.adapter.CreateCategorySpinerAdapter;
import com.e_vippos.base.CommonActivity;
import com.e_vippos.commons.Commons;
import com.e_vippos.utils.CustomSquareLinearLayout;

public class CreateItemActivity extends CommonActivity implements View.OnClickListener {

    CreateCategorySpinerAdapter adapter;

    Animation hyper_show, hyper_hide;

    Spinner ui_spCategory;
    ImageView ui_imvBack, ui_imvEach, ui_imvWeight, ui_imvColor, ui_imvImage, ui_imvStar;
    ImageView ui_imvBg1, ui_imvBg2, ui_imvBg3, ui_imvBg4, ui_imvBg5, ui_imvBg6, ui_imvBg7, ui_imvBg8, ui_imvShape1, ui_imvShape2, ui_imvShape3, ui_imvShape4, ui_imvSave;
    EditText ui_edtItemName;
    LinearLayout ui_activity_create_item, ui_lytSave;
    CustomSquareLinearLayout ui_sqL1, ui_sqL2, ui_sqL3, ui_sqL4, ui_sqL5, ui_sqL6, ui_sqL7, ui_sqL8, ui_sqL9, ui_sqL10, ui_sqL11, ui_sqL12;
    TextView ui_txvSave;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_item);

        loadLayout();
    }

    private void loadLayout() {

        ui_imvBg1 = (ImageView)findViewById(R.id.imvBg1);
        ui_imvBg2 = (ImageView)findViewById(R.id.imvBg2);
        ui_imvBg3 = (ImageView)findViewById(R.id.imvBg3);
        ui_imvBg4 = (ImageView)findViewById(R.id.imvBg4);
        ui_imvBg5 = (ImageView)findViewById(R.id.imvBg5);
        ui_imvBg6 = (ImageView)findViewById(R.id.imvBg6);
        ui_imvBg7 = (ImageView)findViewById(R.id.imvBg7);
        ui_imvBg8 = (ImageView)findViewById(R.id.imvBg8);

        ui_imvShape1 = (ImageView)findViewById(R.id.imvShape1);
        ui_imvShape2 = (ImageView)findViewById(R.id.imvShape2);
        ui_imvShape3 = (ImageView)findViewById(R.id.imvShape3);
        ui_imvShape4 = (ImageView)findViewById(R.id.imvShape4);

        ui_sqL1 = (CustomSquareLinearLayout)findViewById(R.id.sqL1);
        ui_sqL1.setOnClickListener(this);

        ui_sqL2 = (CustomSquareLinearLayout)findViewById(R.id.sqL2);
        ui_sqL2.setOnClickListener(this);

        ui_sqL3 = (CustomSquareLinearLayout)findViewById(R.id.sqL3);
        ui_sqL3.setOnClickListener(this);

        ui_sqL4 = (CustomSquareLinearLayout)findViewById(R.id.sqL4);
        ui_sqL4.setOnClickListener(this);

        ui_sqL5 = (CustomSquareLinearLayout)findViewById(R.id.sqL5);
        ui_sqL5.setOnClickListener(this);

        ui_sqL6 = (CustomSquareLinearLayout)findViewById(R.id.sqL6);
        ui_sqL6.setOnClickListener(this);

        ui_sqL7 = (CustomSquareLinearLayout)findViewById(R.id.sqL7);
        ui_sqL7.setOnClickListener(this);

        ui_sqL8 = (CustomSquareLinearLayout)findViewById(R.id.sqL8);
        ui_sqL8.setOnClickListener(this);

        ui_sqL9 = (CustomSquareLinearLayout)findViewById(R.id.sqL9);
        ui_sqL9.setOnClickListener(this);

        ui_sqL10 = (CustomSquareLinearLayout)findViewById(R.id.sqL10);
        ui_sqL10.setOnClickListener(this);

        ui_sqL11 = (CustomSquareLinearLayout)findViewById(R.id.sqL11);
        ui_sqL11.setOnClickListener(this);

        ui_sqL12 = (CustomSquareLinearLayout)findViewById(R.id.sqL12);
        ui_sqL12.setOnClickListener(this);

        ui_imvColor = (ImageView)findViewById(R.id.imvColor);
        ui_imvColor.setOnClickListener(this);

        ui_imvImage = (ImageView)findViewById(R.id.imvImage);
        ui_imvImage.setOnClickListener(this);

        ui_imvEach = (ImageView)findViewById(R.id.imvEach);
        ui_imvEach.setOnClickListener(this);

        ui_imvWeight = (ImageView)findViewById(R.id.imvWeight);
        ui_imvWeight.setOnClickListener(this);

        ui_edtItemName = (EditText)findViewById(R.id.edtItemName);

        ui_imvBack = (ImageView)findViewById(R.id.imvBack);
        ui_imvBack.setOnClickListener(this);

        adapter = new CreateCategorySpinerAdapter(this);
        ui_spCategory = (Spinner)findViewById(R.id.spCategory);
        ui_spCategory.setAdapter(adapter);

        ui_imvStar = (ImageView)findViewById(R.id.imvStar);
        ui_imvStar.setOnClickListener(this);

        ui_lytSave = (LinearLayout)findViewById(R.id.lytSave);
        ui_lytSave.setOnClickListener(this);

        ui_imvSave = (ImageView)findViewById(R.id.imvSave);

        ui_txvSave = (TextView)findViewById(R.id.txvSave);

        ui_activity_create_item = (LinearLayout)findViewById(R.id.activity_create_item);
        ui_activity_create_item.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edtItemName.getWindowToken(), 0);
                return false;
            }
        });

        hyper_show = AnimationUtils.loadAnimation(this, R.anim.anim_btn_publish_feed_show);
        hyper_hide = AnimationUtils.loadAnimation(this,R.anim.anim_btn_publish_feed_hide);
    }

    private void initPOSIcon() {

        ui_imvBg1.setVisibility(View.INVISIBLE);
        ui_imvBg2.setVisibility(View.INVISIBLE);
        ui_imvBg3.setVisibility(View.INVISIBLE);
        ui_imvBg4.setVisibility(View.INVISIBLE);
        ui_imvBg5.setVisibility(View.INVISIBLE);
        ui_imvBg6.setVisibility(View.INVISIBLE);
        ui_imvBg7.setVisibility(View.INVISIBLE);
        ui_imvBg8.setVisibility(View.INVISIBLE);
    }

    private void initShapeIcon() {

        ui_imvShape1.setVisibility(View.INVISIBLE);
        ui_imvShape2.setVisibility(View.INVISIBLE);
        ui_imvShape3.setVisibility(View.INVISIBLE);
        ui_imvShape4.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imvBack:
                finish();
                break;

            case R.id.imvEach:
                ui_imvEach.setImageResource(R.drawable.ic_radio_on);
                ui_imvEach.startAnimation(hyper_show);
                ui_imvWeight.setImageResource(R.drawable.ic_radio_off);
                ui_imvWeight.startAnimation(hyper_hide);
                break;

            case R.id.imvWeight:
                ui_imvWeight.setImageResource(R.drawable.ic_radio_on);
                ui_imvWeight.startAnimation(hyper_show);
                ui_imvEach.setImageResource(R.drawable.ic_radio_off);
                ui_imvEach.startAnimation(hyper_hide);
                break;

            case R.id.imvColor:
                ui_imvColor.setImageResource(R.drawable.ic_radio_on);
                ui_imvColor.startAnimation(hyper_show);
                ui_imvImage.setImageResource(R.drawable.ic_radio_off);
                ui_imvImage.startAnimation(hyper_hide);
                break;

            case R.id.imvImage:
                ui_imvImage.setImageResource(R.drawable.ic_radio_on);
                ui_imvImage.startAnimation(hyper_show);
                ui_imvColor.setImageResource(R.drawable.ic_radio_off);
                ui_imvColor.startAnimation(hyper_hide);
                break;

            case R.id.sqL1:
                initPOSIcon();
                ui_imvBg1.setVisibility(View.VISIBLE);
                break;

            case R.id.sqL2:
                initPOSIcon();
                ui_imvBg2.setVisibility(View.VISIBLE);
                break;

            case R.id.sqL3:
                initPOSIcon();
                ui_imvBg3.setVisibility(View.VISIBLE);
                break;

            case R.id.sqL4:
                initPOSIcon();
                ui_imvBg4.setVisibility(View.VISIBLE);
                break;

            case R.id.sqL5:
                initPOSIcon();
                ui_imvBg5.setVisibility(View.VISIBLE);
                break;

            case R.id.sqL6:
                initPOSIcon();
                ui_imvBg6.setVisibility(View.VISIBLE);
                break;

            case R.id.sqL7:
                initPOSIcon();
                ui_imvBg7.setVisibility(View.VISIBLE);
                break;

            case R.id.sqL8:
                initPOSIcon();
                ui_imvBg8.setVisibility(View.VISIBLE);
                break;

            case R.id.sqL9:
                initShapeIcon();
                ui_imvShape1.setVisibility(View.VISIBLE);
                break;
            case R.id.sqL10:
                initShapeIcon();
                ui_imvShape2.setVisibility(View.VISIBLE);
                break;

            case R.id.sqL11:
                initShapeIcon();
                ui_imvShape3.setVisibility(View.VISIBLE);
                break;

            case R.id.sqL12:
                initShapeIcon();
                ui_imvShape4.setVisibility(View.VISIBLE);
                break;

            case R.id.imvStar:
                ui_imvStar.setSelected(!ui_imvStar.isSelected());
                ui_imvStar.setEnabled(true);
                break;

            case R.id.lytSave:
                ui_imvSave.setAlpha((float) 1.0);
                ui_txvSave.setAlpha((float) 1.0);
                Commons.itemFlag = true;
                break;

        }
    }

}
