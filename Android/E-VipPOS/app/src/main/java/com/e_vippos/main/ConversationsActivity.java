package com.e_vippos.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.e_vippos.R;
import com.e_vippos.adapter.ConversationsAdapter;
import com.e_vippos.base.CommonActivity;

public class ConversationsActivity extends CommonActivity implements View.OnClickListener {

    ConversationsAdapter adapter;

    ListView ui_lstView;

    ImageView ui_imvClose, ui_imvPen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversations);

        loadLayout();
    }

    private void loadLayout() {

        ui_imvClose = (ImageView)findViewById(R.id.imvClose);
        ui_imvClose.setOnClickListener(this);

        ui_imvPen = (ImageView)findViewById(R.id.imvPen);
        ui_imvPen.setOnClickListener(this);

        adapter = new ConversationsAdapter(this);
        ui_lstView = (ListView)findViewById(R.id.lstView);
        ui_lstView.setAdapter(adapter);
    }

    private void gotoNewConversationActivity() {

        Intent intent = new Intent(this, NewConversationActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imvClose:
                finish();
                break;

            case R.id.imvPen:
                gotoNewConversationActivity();
                break;
        }
    }
}
