package com.e_vippos.main;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.e_vippos.R;
import com.e_vippos.adapter.CountrySpinnerAdapter;
import com.e_vippos.base.CommonActivity;

public class SignupActivity extends CommonActivity implements View.OnClickListener {

    CountrySpinnerAdapter adapter;

    String email;

    LinearLayout ui_activity_signup;
    ImageView ui_imv_back, ui_imv_check;
    Spinner ui_spinner_country;
    EditText ui_edt_email;
    TextView ui_txv_continue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        loadLayout();
    }

    private void loadLayout() {

        ui_txv_continue = (TextView)findViewById(R.id.txv_continue);
        ui_txv_continue.setOnClickListener(this);

        ui_edt_email = (EditText)findViewById(R.id.edt_email);

        adapter = new CountrySpinnerAdapter(this);
        ui_spinner_country = (Spinner)findViewById(R.id.spinner_country);
        ui_spinner_country.setAdapter(adapter);


        ui_imv_check = (ImageView)findViewById(R.id.imv_check);
        ui_imv_check.setOnClickListener(this);

        ui_imv_back = (ImageView)findViewById(R.id.imvBack);
        ui_imv_back.setOnClickListener(this);

        ui_activity_signup = (LinearLayout)findViewById(R.id.activity_signup);
        ui_activity_signup.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edt_email.getWindowToken(), 0);
                return false;
            }
        });
    }

    private void gotoLoginActivity() {

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);

        finish();
    }

    public void showAlert() {

        email = ui_edt_email.getText().toString();

        String message = "Thank you for registering! Please confirm the correctness of your email address. A confirmation letter has been sent to for " + email;


        final AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(message);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "continue",

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        alertDialog.cancel();

                        Intent intent = new Intent(SignupActivity.this, WelcomeActivity.class);
                        startActivity(intent);

                        finish();
                    }
                });

        alertDialog.show();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imvBack:
                gotoLoginActivity();
                break;

            case R.id.imv_check:
                ui_imv_check.setSelected(!ui_imv_check.isSelected());
                ui_imv_check.setEnabled(true);
                break;

            case R.id.txv_continue:
                showAlert();
                break;
        }
    }

}
