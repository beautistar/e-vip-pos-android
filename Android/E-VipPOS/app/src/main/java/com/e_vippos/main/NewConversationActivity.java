package com.e_vippos.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.e_vippos.R;
import com.e_vippos.base.CommonActivity;
import com.e_vippos.commons.Constants;
import com.e_vippos.filepanda.FileListActivity;

public class NewConversationActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvBack, ui_imvAttachment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_conversation);

        loadlayout();
    }

    private void loadlayout() {

        ui_imvAttachment = (ImageView)findViewById(R.id.imvAttachment);
        ui_imvAttachment.setOnClickListener(this);

        ui_imvBack = (ImageView)findViewById(R.id.imvBack);
        ui_imvBack.setOnClickListener(this);
    }

    private void gotoFileListActivity() {

        Intent intent =  new Intent(this, FileListActivity.class);
        startActivityForResult(intent, Constants.PICK_FROM_FILE);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imvBack:
                finish();
                break;

            case R.id.imvAttachment:
                gotoFileListActivity();
                break;
        }
    }

}
