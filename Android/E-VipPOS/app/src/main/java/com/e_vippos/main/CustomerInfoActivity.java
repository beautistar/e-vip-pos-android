package com.e_vippos.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.e_vippos.R;
import com.e_vippos.base.CommonActivity;

public class CustomerInfoActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvBack, ui_imvEdit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_info);

        loadLayout();
    }

    private void loadLayout() {

        ui_imvEdit = (ImageView)findViewById(R.id.imvEdit);
        ui_imvEdit.setOnClickListener(this);

        ui_imvBack = (ImageView)findViewById(R.id.imvBack);
        ui_imvBack.setOnClickListener(this);
    }

    private void gotoEditCustomerActivity() {

        Intent intent = new Intent(this, EditCustomerActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imvEdit:
                gotoEditCustomerActivity();
                break;

            case R.id.imvBack:
                finish();
                break;
        }
    }

}
