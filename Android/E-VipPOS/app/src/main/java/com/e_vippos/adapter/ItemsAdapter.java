package com.e_vippos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.e_vippos.R;
import com.e_vippos.main.ItemsActivity;

/**
 * Created by STS on 10/24/2016.
 */

public class ItemsAdapter extends BaseAdapter {

    ItemsActivity activity;

    View view;
    TextView ui_txvItem;

    public ItemsAdapter(ItemsActivity activity) {
        this.activity = activity;
    }

    String[] items_name = {"All categories", "Android", "iPhone"};

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater =  (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        view =  inflater.inflate(R.layout.item_items, parent, false);

        loadLayout(position);

        return view;
    }

    private void loadLayout(int position) {

        ui_txvItem = (TextView)view.findViewById(R.id.txvItem);
        ui_txvItem.setText(items_name[position]);
    }
}
