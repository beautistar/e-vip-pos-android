package com.e_vippos.main;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;

import com.e_vippos.R;
import com.e_vippos.base.CommonActivity;
import com.e_vippos.commons.Constants;

public class WelcomeActivity extends CommonActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wecome);

        showProgress();
        loadLayout();
    }

    private void loadLayout() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(WelcomeActivity.this, MainScreenActivity.class);

                startActivity(intent);
                finish();

                closeProgress();
            }
        }, 1000);
    }
}
