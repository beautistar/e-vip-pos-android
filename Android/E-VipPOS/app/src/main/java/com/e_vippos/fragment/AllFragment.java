package com.e_vippos.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.e_vippos.R;
import com.e_vippos.adapter.AllFragmentAdapter;
import com.e_vippos.base.BaseFragment;
import com.e_vippos.commons.Commons;
import com.e_vippos.main.ConversationsActivity;
import com.e_vippos.main.CreateItemActivity;
import com.e_vippos.main.MainScreenActivity;

/**
 * Created by STS on 10/23/2016.
 */

@SuppressLint("ValidFragment")
public class AllFragment extends BaseFragment implements View.OnClickListener{

    AllFragmentAdapter adapter;

    MainScreenActivity activity;

    View view ;
    TextView ui_txvOK;
    LinearLayout ui_lytNoItems;
    ListView ui_lstAll;

    public AllFragment(MainScreenActivity activity) {
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_all, container, false);

        loadLayout();

        return view;
    }

    private void loadLayout() {

        ui_lytNoItems = (LinearLayout)view.findViewById(R.id.lytNoItems);

        ui_lstAll = (ListView)view.findViewById(R.id.lstAll);

        if (!Commons.itemFlag){

            ui_lstAll.setVisibility(View.GONE);

            ui_txvOK = (TextView)view.findViewById(R.id.txvOK);
            ui_txvOK.setOnClickListener(this);

        }else {

            ui_lytNoItems.setVisibility(View.GONE);

            ui_lstAll.setVisibility(View.VISIBLE);

            adapter = new AllFragmentAdapter(activity);
            ui_lstAll.setAdapter(adapter);
            ui_lstAll.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    int cnt =Integer.valueOf(activity.ui_txvTicketCnt.getText().toString());
                    cnt++;
                    activity.ui_txvTicketCnt.setText(String.valueOf(cnt));

                    Toast.makeText(activity, "Your Tickets are " + String.valueOf(cnt), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void gotoCreateItemActivity() {

        Intent intent = new Intent(activity, CreateItemActivity.class);
        activity.startActivity(intent);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.txvOK:
                gotoCreateItemActivity();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        loadLayout();
    }
}
