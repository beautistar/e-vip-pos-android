package com.e_vippos.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.e_vippos.R;
import com.e_vippos.main.CreateCategoryActivity;
import com.e_vippos.main.CreateItemActivity;

/**
 * Created by STS on 10/24/2016.
 */

public class CreateCategorySpinerAdapter extends BaseAdapter {

    CreateItemActivity activity;

    public CreateCategorySpinerAdapter(CreateItemActivity activity) {
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater =  (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (position == 0){

            convertView = inflater.inflate(R.layout.item_category1, parent, false);
        }else if (position == 1){

            convertView = inflater.inflate(R.layout.item_category2, parent, false);

        }else {

            convertView = inflater.inflate(R.layout.item_category3, parent, false);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (position == 1){

                    gotoCreateCategoryActivity();
                }
            }
        });

        return convertView;
    }

    private void gotoCreateCategoryActivity() {

        Intent intent = new Intent(activity, CreateCategoryActivity.class);
        activity.startActivity(intent);
    }
}
