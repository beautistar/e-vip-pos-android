package com.e_vippos.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.e_vippos.R;
import com.e_vippos.main.SignupActivity;

import java.util.Locale;

/**
 * Created by STS on 10/23/2016.
 */

public class CountrySpinnerAdapter extends BaseAdapter {

//    SignupActivity activity;

    private final Context context;

    String[] country_name;

    public CountrySpinnerAdapter(Context activity) {
        this.context = activity;

        country_name = context.getResources().getStringArray(R.array.CountryCodes);
    }

    @Override
    public int getCount() {
        return country_name.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater =  (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.item_country, parent, false);

        ImageView ui_imv_flag = (ImageView)convertView.findViewById(R.id.imv_flag);
        TextView ui_txv_name = (TextView)convertView.findViewById(R.id.txv_name);

        String[] g=country_name[position].split(",");
        ui_txv_name.setText(GetCountryZipCode(g[1]).trim());

        String pngName = g[1].trim().toLowerCase();
        ui_imv_flag.setImageResource(context.getResources().getIdentifier("drawable/ic_flag_flat_" + pngName, null, context.getPackageName()));

        return convertView;
    }

    private String GetCountryZipCode(String ssid){

        Locale loc = new Locale("", ssid);
        return loc.getDisplayCountry().trim();
    }
}
