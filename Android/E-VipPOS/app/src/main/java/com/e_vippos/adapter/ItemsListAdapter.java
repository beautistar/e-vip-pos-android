package com.e_vippos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.e_vippos.R;
import com.e_vippos.main.ItemsActivity;

/**
 * Created by STS on 10/24/2016.
 */

public class ItemsListAdapter extends BaseAdapter {

    ItemsActivity activity;

    public ItemsListAdapter(ItemsActivity activity) {
        this.activity = activity;
    }


    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater =  (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convertView =  inflater.inflate(R.layout.item_items_list, parent, false);

        final ImageView ui_imvStar = (ImageView)convertView.findViewById(R.id.imvStar);
        ui_imvStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ui_imvStar.setSelected(!ui_imvStar.isSelected());
                ui_imvStar.setEnabled(true);
            }
        });

        return convertView;
    }
}
