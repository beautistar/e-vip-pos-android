package com.e_vippos.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.e_vippos.R;
import com.e_vippos.base.BaseFragment;
import com.e_vippos.main.CreateItemActivity;
import com.e_vippos.main.ItemsActivity;
import com.e_vippos.main.MainScreenActivity;

/**
 * Created by STS on 10/23/2016.
 */

@SuppressLint("ValidFragment")
public class FavorityFragment extends BaseFragment implements View.OnClickListener {

    MainScreenActivity activity;

    View view;
    TextView ui_txvOK;

    public FavorityFragment(MainScreenActivity activity) {
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_favorites, container, false);

        loadLayout();

        return view;
    }

    private void loadLayout() {

        ui_txvOK = (TextView)view.findViewById(R.id.txvOK);
        ui_txvOK.setOnClickListener(this);
    }

    private void gotoItemsActivity() {

        Intent intent = new Intent(activity,ItemsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.txvOK:
                gotoItemsActivity();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        loadLayout();
    }
}
