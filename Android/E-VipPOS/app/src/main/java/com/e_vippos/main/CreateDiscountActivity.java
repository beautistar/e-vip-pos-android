package com.e_vippos.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.e_vippos.R;
import com.e_vippos.base.CommonActivity;

public class CreateDiscountActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvBack, ui_imvSave;
    LinearLayout ui_lytSave;
    TextView ui_txvSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_discount);

        loadLayout();
    }

    private void loadLayout() {

        ui_imvSave = (ImageView)findViewById(R.id.imvSave);
        ui_txvSave = (TextView)findViewById(R.id.txvSave);

        ui_lytSave = (LinearLayout)findViewById(R.id.lytSave);
        ui_lytSave.setOnClickListener(this);

        ui_imvBack = (ImageView)findViewById(R.id.imvBack);
        ui_imvBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.lytSave:
                ui_imvSave.setAlpha((float)1.0);
                ui_txvSave.setAlpha((float)1.0);
                break;

            case R.id.imvBack:
                finish();
                break;
        }
    }
}
