package com.e_vippos.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.e_vippos.R;
import com.e_vippos.adapter.CategoryAdapter;
import com.e_vippos.base.BaseFragment;
import com.e_vippos.commons.Commons;
import com.e_vippos.main.CategoriesActivity;
import com.e_vippos.main.CreateItemActivity;
import com.e_vippos.main.DiscountsActivity;
import com.e_vippos.main.ItemsActivity;
import com.e_vippos.main.MainScreenActivity;

/**
 * Created by STS on 10/23/2016.
 */

@SuppressLint("ValidFragment")
public class ItemsFragment extends BaseFragment implements View.OnClickListener {

    MainScreenActivity activity;

    View view;
    LinearLayout ui_lytItems, ui_lytCategory, ui_lytDiscounts;

    public ItemsFragment(MainScreenActivity activity) {
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view =  inflater.inflate(R.layout.fragment_items, container, false);

        loadLayout();

        return view;
    }

    private void loadLayout() {

        ui_lytItems = (LinearLayout)view.findViewById(R.id.lytItems);
        ui_lytItems.setOnClickListener(this);

        ui_lytCategory = (LinearLayout)view.findViewById(R.id.lytCategory);
        ui_lytCategory.setOnClickListener(this);

        ui_lytDiscounts = (LinearLayout)view.findViewById(R.id.lytDiscounts);
        ui_lytDiscounts.setOnClickListener(this);
    }

    private void gotoCreateItemActivity() {

        if (!Commons.itemFlag){

            Intent intent = new Intent(activity, CreateItemActivity.class);
            activity.startActivity(intent);

        }else {

            Intent intent = new Intent(activity, ItemsActivity.class);
            activity.startActivity(intent);
        }
    }

    private void gotoCategoriesActivity() {

        Intent intent =  new Intent(activity, CategoriesActivity.class);
        activity.startActivity(intent);
    }

    private void gotoDiscountsActivity() {

        Intent intent =  new Intent(activity, DiscountsActivity.class);
        activity.startActivity(intent);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.lytItems:
                gotoCreateItemActivity();
                break;

            case R.id.lytCategory:
                gotoCategoriesActivity();
                break;

            case R.id.lytDiscounts:
                gotoDiscountsActivity();
                break;
        }
    }

}
