package com.e_vippos.main;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.e_vippos.R;
import com.e_vippos.base.CommonActivity;

public class LoginActivity extends CommonActivity implements View.OnClickListener {

    LinearLayout ui_activity_login;
    TextView ui_txv_login, ui_txv_forgotten, ui_txv_registration;
    EditText ui_edt_password;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loadLayout();
    }

    private void loadLayout() {

        ui_txv_registration = (TextView)findViewById(R.id.txv_registration);
        ui_txv_registration.setOnClickListener(this);

        ui_txv_forgotten = (TextView)findViewById(R.id.txv_forgotten);
        ui_txv_forgotten.setVisibility(View.VISIBLE);

        ui_txv_login = (TextView)findViewById(R.id.txv_login);
        ui_txv_login.setOnClickListener(this);

        ui_edt_password = (EditText)findViewById(R.id.edt_password);
        ui_edt_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (ui_edt_password.getText().toString().length() > 0){
                    ui_txv_forgotten.setVisibility(View.INVISIBLE);
                }else {
                    ui_txv_forgotten.setVisibility(View.VISIBLE);
                }
            }
        });

        ui_activity_login = (LinearLayout)findViewById(R.id.activity_login);
        ui_activity_login.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edt_password.getWindowToken(), 0);
                return false;
            }
        });
    }

    private void gotoSignupActivity() {

        Intent intent =  new Intent(this, SignupActivity.class);
        startActivity(intent);

        finish();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.txv_registration:
                gotoSignupActivity();
                break;
        }

    }



}
