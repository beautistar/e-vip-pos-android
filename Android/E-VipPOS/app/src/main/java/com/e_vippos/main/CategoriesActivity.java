package com.e_vippos.main;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.e_vippos.R;
import com.e_vippos.adapter.CategoriesAdapter;
import com.e_vippos.base.CommonActivity;

public class CategoriesActivity extends CommonActivity implements View.OnClickListener {

    CategoriesAdapter adapter;

    ImageView ui_imvBack;
    ListView ui_lstCategories;
    FloatingActionButton ui_fab;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        loadLayout();
    }

    private void loadLayout() {

        adapter = new CategoriesAdapter(this);
        ui_lstCategories = (ListView)findViewById(R.id.lstCategories);
        ui_lstCategories.setAdapter(adapter);

        ui_imvBack = (ImageView)findViewById(R.id.imvBack);
        ui_imvBack.setOnClickListener(this);

        ui_fab = (FloatingActionButton)findViewById(R.id.fab);
        ui_fab.setOnClickListener(this);
    }

    private void gotoCreateCategoryActivity() {

        Intent intent =  new Intent(this, CreateCategoryActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imvBack:
                finish();
                break;

            case R.id.fab:
                gotoCreateCategoryActivity();
                break;
        }
    }
}
