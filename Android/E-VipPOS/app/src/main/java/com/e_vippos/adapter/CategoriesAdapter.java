package com.e_vippos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.e_vippos.R;
import com.e_vippos.main.CategoriesActivity;

/**
 * Created by STS on 10/24/2016.
 */

public class CategoriesAdapter extends BaseAdapter {

    CategoriesActivity activity;

    public CategoriesAdapter(CategoriesActivity activity) {
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater =  (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.item_categories, parent, false);

        return convertView;
    }
}
