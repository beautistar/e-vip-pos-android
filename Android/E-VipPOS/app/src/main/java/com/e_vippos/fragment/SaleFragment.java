package com.e_vippos.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.e_vippos.R;
import com.e_vippos.adapter.CategoryAdapter;
import com.e_vippos.base.BaseFragment;
import com.e_vippos.main.MainScreenActivity;

/**
 * Created by STS on 10/23/2016.
 */

@SuppressLint("ValidFragment")
public class SaleFragment extends BaseFragment implements View.OnClickListener{

    MainScreenActivity activity;

    CategoryAdapter adapter;

    View view;
    TabLayout ui_tabLayout;
    ViewPager ui_viewPager;
    ImageView ui_imvAll, ui_imvFavorites;
    TextView ui_txvAll, ui_txvFavorites;
    LinearLayout ui_lytAll, ui_lytFavorites;

    public SaleFragment(MainScreenActivity activity) {
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view =  inflater.inflate(R.layout.fragment_sales, container, false);

        loadLayout();

        return view;
    }

    private void loadLayout() {

        ui_lytAll = (LinearLayout)view.findViewById(R.id.lytAll);
        ui_lytAll.setOnClickListener(this);

        ui_lytFavorites = (LinearLayout)view.findViewById(R.id.lytFavorites);
        ui_lytFavorites.setOnClickListener(this);

        ui_imvAll = (ImageView)view.findViewById(R.id.imvAll);
        ui_txvAll = (TextView)view.findViewById(R.id.txvAll);

        ui_imvFavorites = (ImageView)view.findViewById(R.id.imvFavorites);
        ui_txvFavorites = (TextView)view.findViewById(R.id.txvFavorites);

        ui_tabLayout = (TabLayout)view.findViewById(R.id.tabLayout);

        ui_viewPager = (ViewPager)view.findViewById(R.id.viewPager);
        FragmentManager manager = getChildFragmentManager();
        adapter = new CategoryAdapter(activity, manager);
        ui_viewPager.setAdapter(adapter);

        initIcons();
        ui_imvAll.setImageResource(R.drawable.ic_list_small_grn);
        ui_txvAll.setTextColor(getResources().getColor(R.color.colorGreen));

        ui_tabLayout.setupWithViewPager(ui_viewPager);

        ui_viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageScrollStateChanged(int state) {}

            @Override
            public void onPageSelected(int position) {

                setTab(position);
            }
        });
    }

    private void setTab(int position) {

        switch (position){

            case 0:
                initIcons();
                ui_imvAll.setImageResource(R.drawable.ic_list_small_grn);
                ui_txvAll.setTextColor(getResources().getColor(R.color.colorGreen));
                break;

            case 1:
                initIcons();
                ui_imvFavorites.setImageResource(R.drawable.ic_star_small_grm);
                ui_txvFavorites.setTextColor(getResources().getColor(R.color.colorGreen));
                break;
        }
    }

    private void initIcons() {

        ui_imvAll.setImageResource(R.drawable.ic_list_small);
        ui_txvAll.setTextColor(getResources().getColor(R.color.colorDarkGrey));

        ui_imvFavorites.setImageResource(R.drawable.ic_star_small);
        ui_txvFavorites.setTextColor(getResources().getColor(R.color.colorDarkGrey));
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.lytAll:
                setTab(0);
                ui_viewPager.setCurrentItem(0);
                break;

            case R.id.lytFavorites:
                setTab(1);
                ui_viewPager.setCurrentItem(1);
                break;
        }
    }
}
