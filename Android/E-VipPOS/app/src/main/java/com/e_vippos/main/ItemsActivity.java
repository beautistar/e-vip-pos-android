package com.e_vippos.main;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;

import com.e_vippos.R;
import com.e_vippos.adapter.ItemsAdapter;
import com.e_vippos.adapter.ItemsListAdapter;
import com.e_vippos.base.CommonActivity;

public class ItemsActivity extends CommonActivity implements View.OnClickListener {

    ItemsAdapter adapter;

    ItemsListAdapter itemsListAdapter;

    Spinner ui_spCategory;
    ImageView ui_imvBack;
    ListView ui_lstItems;
    FloatingActionButton ui_fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items);

        loadLayout();
    }

    private void loadLayout() {

        ui_imvBack = (ImageView)findViewById(R.id.imvBack);
        ui_imvBack.setOnClickListener(this);

        adapter = new ItemsAdapter(this);
        ui_spCategory = (Spinner)findViewById(R.id.spCategory);
        ui_spCategory.setAdapter(adapter);

        itemsListAdapter  = new ItemsListAdapter(this);
        ui_lstItems = (ListView)findViewById(R.id.lstItems);
        ui_lstItems.setAdapter(itemsListAdapter);

        ui_fab = (FloatingActionButton)findViewById(R.id.fab);
        ui_fab.setOnClickListener(this);
    }

    private void gotoCreateActivity() {

        Intent intent = new Intent(this, CreateItemActivity.class);
        startActivity(intent);
    }
    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imvBack:
                finish();
                break;

            case R.id.fab:
                gotoCreateActivity();
                break;
        }
    }
}
