package com.e_vippos.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.e_vippos.fragment.AllFragment;
import com.e_vippos.fragment.FavorityFragment;
import com.e_vippos.main.MainScreenActivity;

/**
 * Created by STS on 10/23/2016.
 */

public class CategoryAdapter extends FragmentStatePagerAdapter {

    MainScreenActivity activity;

    public CategoryAdapter(MainScreenActivity activity, FragmentManager fm) {
        super(fm);
        this.activity = activity;
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null;

        switch (position){

            case 0:
                fragment = new AllFragment(activity);
                break;

            case 1:
                fragment = new FavorityFragment(activity);
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
