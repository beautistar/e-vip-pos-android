package com.e_vippos.main;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.e_vippos.R;
import com.e_vippos.adapter.NaviRightAdapter;
import com.e_vippos.base.CommonActivity;
import com.e_vippos.commons.Commons;
import com.e_vippos.fragment.CustomerCareFragment;
import com.e_vippos.fragment.ItemsFragment;
import com.e_vippos.fragment.ReceiptsFragment;
import com.e_vippos.fragment.SaleFragment;
import com.e_vippos.fragment.SettingsFragments;
import com.e_vippos.utils.ToolbarCaptureActivity;
import com.google.zxing.integration.android.IntentIntegrator;

public class MainScreenActivity extends CommonActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener{

    NaviRightAdapter adapter;

    private PopupWindow popupAdd;
    private PopupWindow popupSearch;
    private PopupWindow popupReceipt;
    private PopupWindow popupSale;
    private PopupWindow popupClient;

    private LinearLayout ui_lytContainer, ui_lytTopBar;

    NavigationView ui_naviLeft, ui_naviRight;
    DrawerLayout ui_drawerlayout;
    ActionBarDrawerToggle leftNaviToggle, rightNaviToggle;
    View leftView, rightView, ui_view;

    ImageView ui_imv_call_drawer, ui_imvAddClient, ui_imvSearch;
    TextView ui_txvTicket;
    public TextView ui_txvTicketCnt;
    RelativeLayout ui_rytTicket;
    ListView ui_lstCategories;

    Boolean receipt_flag = false, sale_flag = false, livingActivity = false;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);

        livingActivity = true;
        loadLayout();
    }

    private void loadLayout() {

        ui_view = (View)findViewById(R.id.view);

        ui_lytTopBar = (LinearLayout)findViewById(R.id.lytTopBar);

        ui_txvTicketCnt = (TextView)findViewById(R.id.txvTicketCnt);

        ui_imvSearch = (ImageView)findViewById(R.id.imvSearch);
        ui_imvSearch.setOnClickListener(this);

        ui_imvAddClient = (ImageView)findViewById(R.id.imvAddClient);
        ui_imvAddClient.setOnClickListener(this);

        if (Commons.addClientFlag){
            ui_imvAddClient.setImageResource(R.drawable.ic_client_added);
        }else {
            ui_imvAddClient.setImageResource(R.drawable.ic_client_add);
        }

        ui_rytTicket = (RelativeLayout)findViewById(R.id.rytTicket);
        ui_rytTicket.setOnClickListener(this);

        ui_txvTicket = (TextView)findViewById(R.id.txvTicket);

        ui_imv_call_drawer = (ImageView)findViewById(R.id.imv_call_drawer);
        ui_imv_call_drawer.setOnClickListener(this);

        ui_naviLeft = (NavigationView)findViewById(R.id.naviLeft);
        ui_naviLeft.setNavigationItemSelectedListener(this);
        leftView = ui_naviLeft.getHeaderView(0);

        ui_naviRight = (NavigationView)findViewById(R.id.naviRight);
        rightView = ui_naviRight.getHeaderView(0);

        adapter = new NaviRightAdapter(this);
        ui_lstCategories = (ListView)findViewById(R.id.lstCategories);
        ui_lstCategories.setAdapter(adapter);


        ui_drawerlayout = (DrawerLayout)findViewById(R.id.drawerlayout);

        ui_lytContainer = (LinearLayout)findViewById(R.id.lytContainer);

        setupNavigationBar();

        gotoSalesFragment();
    }

    private void setupNavigationBar() {

        leftNaviToggle = new ActionBarDrawerToggle(this,ui_drawerlayout, R.string.drawer_open, R.string.drawer_close){
            @Override
            public void onDrawerOpened(View drawerView) {

                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {

                super.onDrawerClosed(drawerView);
            }
        };

        ui_drawerlayout.setDrawerListener(leftNaviToggle);
        leftNaviToggle.syncState();

        rightNaviToggle = new ActionBarDrawerToggle(this, ui_drawerlayout, R.string.drawer_open, R.string.drawer_close ){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

        ui_drawerlayout.setDrawerListener(rightNaviToggle);
        rightNaviToggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.left_navi_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    private void gotoSalesFragment() {

        sale_flag = true;
        receipt_flag = false;

        ui_txvTicket.setText(getResources().getString(R.string.ticket));
        ui_rytTicket.setVisibility(View.VISIBLE);
        ui_imvAddClient.setVisibility(View.VISIBLE);
        ui_imvSearch.setVisibility(View.VISIBLE);

        SaleFragment fragment =  new SaleFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frmContainer, fragment);
        fragmentTransaction.commit();
    }

    private void gotoReceiptsFragment() {

        receipt_flag = true;
        sale_flag = false;

        ui_txvTicket.setText(getResources().getString(R.string.receipts));
        ui_rytTicket.setVisibility(View.GONE);
        ui_imvAddClient.setVisibility(View.GONE);
        ui_imvSearch.setVisibility(View.VISIBLE);

        ReceiptsFragment fragment =  new ReceiptsFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frmContainer, fragment);
        fragmentTransaction.commit();
    }

    private void gotoItemsFragmnet() {

        receipt_flag = false;

        ui_txvTicket.setText(getResources().getString(R.string.items));
        ui_rytTicket.setVisibility(View.GONE);
        ui_imvAddClient.setVisibility(View.GONE);
        ui_imvSearch.setVisibility(View.GONE);

        ItemsFragment fragment =  new ItemsFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frmContainer, fragment);
        fragmentTransaction.commit();
    }

    private void gotoCustomerFragment() {

        receipt_flag = false;

        ui_txvTicket.setText(getResources().getString(R.string.customer));
        ui_rytTicket.setVisibility(View.GONE);
        ui_imvAddClient.setVisibility(View.GONE);
        ui_imvSearch.setVisibility(View.GONE);

        CustomerCareFragment fragment =  new CustomerCareFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frmContainer, fragment);
        fragmentTransaction.commit();
    }

    private void gotoSettingsFragments() {

        receipt_flag = false;

        ui_txvTicket.setText(getResources().getString(R.string.settings));
        ui_rytTicket.setVisibility(View.GONE);
        ui_imvAddClient.setVisibility(View.GONE);
        ui_imvSearch.setVisibility(View.GONE);

        SettingsFragments fragment =  new SettingsFragments(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frmContainer, fragment);
        fragmentTransaction.commit();
    }

    private void gotoConversationsActivity() {

        Intent intent = new Intent(this, ConversationsActivity.class);
        startActivity(intent);
    }

    private void gotoAddCustomerActivity() {

        Intent intent = new Intent(this, AddCustomerActivity.class);
        startActivity(intent);
    }

    private void gotoTicketActivity() {

        Intent intent = new Intent(this, TicketActivity.class);
        startActivity(intent);
    }

    private void showPopupAdd() {

        LayoutInflater inflater = (LayoutInflater)_context.getSystemService(LAYOUT_INFLATER_SERVICE);

        View customView = inflater.inflate(R.layout.popup_add,null);

        popupAdd = new PopupWindow(
                customView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );

        popupAdd.showAsDropDown(ui_view);

        LinearLayout ui_lytAddContainer = (LinearLayout)customView.findViewById(R.id.lytAddContainer);
        ui_lytAddContainer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupAdd.dismiss();
                return false;
            }
        });

        LinearLayout ui_lytScan = (LinearLayout)customView.findViewById(R.id.lytScan);
        ui_lytScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupAdd.dismiss();
                new IntentIntegrator(MainScreenActivity.this).setCaptureActivity(ToolbarCaptureActivity.class).initiateScan();
            }
        });

        LinearLayout ui_lytAdd = (LinearLayout)customView.findViewById(R.id.lytAdd);
        ui_lytAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupAdd.dismiss();
                gotoAddCustomerActivity();
            }
        });

        LinearLayout ui_lytSearch = (LinearLayout)customView.findViewById(R.id.lytSearch);
        ui_lytSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupAdd.dismiss();
                showPopupSearch();
            }
        });

    }

    private void showPopupSearch() {

        final InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);

        LayoutInflater inflater = (LayoutInflater)_context.getSystemService(LAYOUT_INFLATER_SERVICE);

        View customView = inflater.inflate(R.layout.popup_search,null);

        popupSearch = new PopupWindow(
                customView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );

        popupSearch.showAsDropDown(ui_view);
        popupSearch.setFocusable(true);
        popupSearch.update();

        LinearLayout ui_lytSearchContainer = (LinearLayout)customView.findViewById(R.id.lytSearchContainer);
        ui_lytSearchContainer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupSearch.dismiss();
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY,0);
                return false;
            }
        });

        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }

    @SuppressLint("NewApi")
    private void showReceiptSearch() {

        final InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);

        LayoutInflater inflater = (LayoutInflater)_context.getSystemService(LAYOUT_INFLATER_SERVICE);

        View customView = inflater.inflate(R.layout.receipt_search,null);

        popupReceipt = new PopupWindow(
                customView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );

        popupReceipt.showAsDropDown(ui_view);
        popupReceipt.setFocusable(true);
        popupReceipt.update();

        LinearLayout ui_lytReceiptContainer = (LinearLayout)customView.findViewById(R.id.lytReceiptContainer);
        ui_lytReceiptContainer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupReceipt.dismiss();
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY,0);
                return false;
            }
        });

        EditText ui_edtReceiptNumber = (EditText)customView.findViewById(R.id.edtReceiptNumber);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }

    private void showSaleSearch() {

        final InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);

        LayoutInflater inflater = (LayoutInflater)_context.getSystemService(LAYOUT_INFLATER_SERVICE);

        View customView = inflater.inflate(R.layout.sale_search,null);

        popupSale = new PopupWindow(
                customView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );

        popupSale.showAsDropDown(ui_view);
        popupSale.setFocusable(true);
        popupSale.update();

        LinearLayout ui_lytSaleContainer = (LinearLayout)customView.findViewById(R.id.lytSaleContainer);
        ui_lytSaleContainer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupSale.dismiss();
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY,0);
                return false;
            }
        });

        EditText ui_edtReceiptNumber = (EditText)customView.findViewById(R.id.edtReceiptNumber);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }

    private void showPopupClient() {

        LayoutInflater inflater = (LayoutInflater)_context.getSystemService(LAYOUT_INFLATER_SERVICE);

        View customView = inflater.inflate(R.layout.popup_client,null);

        popupClient = new PopupWindow(
                customView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );

        popupClient.showAsDropDown(ui_view);

        LinearLayout ui_lytClientContainer = (LinearLayout)customView.findViewById(R.id.lytClientContainer);
        ui_lytClientContainer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupClient.dismiss();
                return false;
            }
        });

        final LinearLayout ui_lytMenu = (LinearLayout)customView.findViewById(R.id.lytMenu);
        ui_lytMenu.setVisibility(View.INVISIBLE);

        ImageView ui_imvMenu = (ImageView)customView.findViewById(R.id.imvMenu);
        ui_imvMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ui_lytMenu.setVisibility(View.VISIBLE);
            }
        });

        TextView ui_txvRemove = (TextView)customView.findViewById(R.id.txvRemove);
        ui_txvRemove.setOnClickListener(this);
        ui_txvRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                popupClient.dismiss();
                Commons.addClientFlag = false;
                loadLayout();
                popupAdd.showAsDropDown(ui_view);
            }
        });

        LinearLayout ui_lytClient = (LinearLayout)customView.findViewById(R.id.lytClient);
        ui_lytClient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gotoCustomerInfoActivity();
            }
        });
    }

    private void gotoCustomerInfoActivity() {

        Intent intent = new Intent(this, CustomerInfoActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        loadLayout();

        if (livingActivity){

            if (Commons.addClientFlag){
                ui_imvAddClient.setImageResource(R.drawable.ic_client_added);
            }else {
                ui_imvAddClient.setImageResource(R.drawable.ic_client_add);
            }
        }

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (item.isChecked()) item.setChecked(false); else item.setChecked(true);
        ui_drawerlayout.closeDrawers();

        switch (id){

            case R.id.sales:
                gotoSalesFragment();
                break;

            case R.id.receipt:
                gotoReceiptsFragment();
                break;

            case R.id.items:
                gotoItemsFragmnet();
                break;

            case R.id.customer:
                gotoCustomerFragment();
                break;

            case R.id.back_office:
                Share("","");
                break;

            case R.id.settings:
                gotoSettingsFragments();
                break;

            case R.id.support:
                gotoConversationsActivity();
                break;
        }

        return true;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_call_drawer:
                ui_drawerlayout.openDrawer(Gravity.LEFT);
                break;

            case R.id.imvAddClient:
                if (Commons.addClientFlag){
                    showPopupClient();
                }else {
                    showPopupAdd();
                }

                break;

            case R.id.rytTicket:
                gotoTicketActivity();
                break;

            case R.id.imvSearch:
                if (receipt_flag){
                    showReceiptSearch();
                }else if (sale_flag){
                    showSaleSearch();
                }
                break;
        }

    }

}
