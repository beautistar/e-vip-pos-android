package com.e_vippos.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.e_vippos.R;
import com.e_vippos.base.CommonActivity;
import com.e_vippos.utils.CustomSquareLinearLayout;

public class CreateCategoryActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvBack;
    CustomSquareLinearLayout ui_sqL1, ui_sqL2, ui_sqL3, ui_sqL4, ui_sqL5, ui_sqL6, ui_sqL7, ui_sqL8;
    ImageView ui_imvBg1, ui_imvBg2, ui_imvBg3, ui_imvBg4, ui_imvBg5, ui_imvBg6, ui_imvBg7, ui_imvBg8;
    LinearLayout ui_lytSave;
    TextView ui_txvSave;
    ImageView ui_imvSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_category);

        loadLayout();
    }

    private void loadLayout() {

        ui_sqL1 = (CustomSquareLinearLayout)findViewById(R.id.sqL1);
        ui_sqL1.setOnClickListener(this);

        ui_sqL2 = (CustomSquareLinearLayout)findViewById(R.id.sqL2);
        ui_sqL2.setOnClickListener(this);

        ui_sqL3 = (CustomSquareLinearLayout)findViewById(R.id.sqL3);
        ui_sqL3.setOnClickListener(this);

        ui_sqL4 = (CustomSquareLinearLayout)findViewById(R.id.sqL4);
        ui_sqL4.setOnClickListener(this);

        ui_sqL5 = (CustomSquareLinearLayout)findViewById(R.id.sqL5);
        ui_sqL5.setOnClickListener(this);

        ui_sqL6 = (CustomSquareLinearLayout)findViewById(R.id.sqL6);
        ui_sqL6.setOnClickListener(this);

        ui_sqL7 = (CustomSquareLinearLayout)findViewById(R.id.sqL7);
        ui_sqL7.setOnClickListener(this);

        ui_sqL8 = (CustomSquareLinearLayout)findViewById(R.id.sqL8);
        ui_sqL8.setOnClickListener(this);

        ui_imvBg1 = (ImageView)findViewById(R.id.imvBg1); ui_imvBg1.setVisibility(View.VISIBLE);
        ui_imvBg2 = (ImageView)findViewById(R.id.imvBg2);
        ui_imvBg3 = (ImageView)findViewById(R.id.imvBg3);
        ui_imvBg4 = (ImageView)findViewById(R.id.imvBg4);
        ui_imvBg5 = (ImageView)findViewById(R.id.imvBg5);
        ui_imvBg6 = (ImageView)findViewById(R.id.imvBg6);
        ui_imvBg7 = (ImageView)findViewById(R.id.imvBg7);
        ui_imvBg8 = (ImageView)findViewById(R.id.imvBg8);

        ui_lytSave = (LinearLayout)findViewById(R.id.lytSave);
        ui_lytSave.setOnClickListener(this);

        ui_txvSave = (TextView)findViewById(R.id.txvSave);
        ui_imvSave = (ImageView)findViewById(R.id.imvSave);

        ui_imvBack = (ImageView)findViewById(R.id.imvBack);
        ui_imvBack.setOnClickListener(this);
    }

    private void initIcon() {

        ui_imvBg1.setVisibility(View.INVISIBLE);
        ui_imvBg2.setVisibility(View.INVISIBLE);
        ui_imvBg3.setVisibility(View.INVISIBLE);
        ui_imvBg4.setVisibility(View.INVISIBLE);
        ui_imvBg5.setVisibility(View.INVISIBLE);
        ui_imvBg6.setVisibility(View.INVISIBLE);
        ui_imvBg7.setVisibility(View.INVISIBLE);
        ui_imvBg8.setVisibility(View.INVISIBLE);
    }
    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imvBack:
                finish();
                break;

            case R.id.sqL1:
                initIcon();
                ui_imvBg1.setVisibility(View.VISIBLE);
                break;

            case R.id.sqL2:
                initIcon();
                ui_imvBg2.setVisibility(View.VISIBLE);
                break;

            case R.id.sqL3:
                initIcon();
                ui_imvBg3.setVisibility(View.VISIBLE);
                break;

            case R.id.sqL4:
                initIcon();
                ui_imvBg4.setVisibility(View.VISIBLE);
                break;

            case R.id.sqL5:
                initIcon();
                ui_imvBg5.setVisibility(View.VISIBLE);
                break;

            case R.id.sqL6:
                initIcon();
                ui_imvBg6.setVisibility(View.VISIBLE);
                break;

            case R.id.sqL7:
                initIcon();
                ui_imvBg7.setVisibility(View.VISIBLE);
                break;

            case R.id.sqL8:
                initIcon();
                ui_imvBg8.setVisibility(View.VISIBLE);
                break;

            case R.id.lytSave:
                ui_imvSave.setAlpha((float)1.0);
                ui_txvSave.setAlpha((float)1.0);
                break;
        }
    }

}
